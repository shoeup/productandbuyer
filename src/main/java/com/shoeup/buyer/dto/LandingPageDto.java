package com.shoeup.buyer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LandingPageDto {
//       "id": 1,
//               "categoryId": 3,
//               "productName": "office shoes",
//               "productImage": null,
//               "color": null,
//               "productDynamicList": [
//    {
//        "id": 1,
//            "staticId": 1,
//            "merchantId": 2,
//            "stock": 4,
//            "price": 4700,
//            "size": null
    Long staticId;
    Long categoryId;
    String productName;
    String description;
    String producImage;
    String color;
    Long merchantId;
    Long stock;
    Long price;
}
