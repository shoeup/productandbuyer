package com.shoeup.buyer.dto;

import com.shoeup.buyer.entity.ProductDynamic;
import com.shoeup.buyer.entity.ProductStatic;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DynamicDto {
    //private ProductDynamic productDynamic;
    @Id
    Long id;
    Long staticId;
    Long merchantId;
    Long stock;
    Long price;
    Long size;
}
