package com.shoeup.buyer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UpdateDto {
    Long productId;
    String productName;
    String Description;
    Long price;
    Long stock;
    String productImage;
    Long size;
    String color;
    Long staticId;
}






