package com.shoeup.buyer.dto;

import com.shoeup.buyer.entity.ProductDynamic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StaticDto {
    @Id
    Long id;
    Long categoryId;
    String productName;
    String Description;
    String productImage;
    String color;
//    @OneToMany(targetEntity = ProductDynamic.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "static_id",referencedColumnName = "id")
    private List<ProductDynamic> productDynamicList;
}
