package com.shoeup.buyer.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MerchantCardDto {
    Long productId;
    String productName;
    String Description;
    Long price;
    Long stock;
    String productImage;
    Long size;
    String color;
}
