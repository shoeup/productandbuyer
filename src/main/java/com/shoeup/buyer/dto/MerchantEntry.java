package com.shoeup.buyer.dto;

import lombok.*;

import javax.persistence.Column;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Setter
public class MerchantEntry {
    Long id;
    String name;
}
