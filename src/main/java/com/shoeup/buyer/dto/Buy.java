package com.shoeup.buyer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Buy {
    Long userId;
    Long dynamicId;
    Long quantity;
    Long price;
    String email;
}
