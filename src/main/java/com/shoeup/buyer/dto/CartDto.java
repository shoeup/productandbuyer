package com.shoeup.buyer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDto {
//    +--------------------+---------+------+-----+---------+----------------+
//            | Field              | Type    | Null | Key | Default | Extra          |
//            +--------------------+---------+------+-----+---------+----------------+
//            | id                 | int(11) | NO   | PRI | NULL    | auto_increment |
//            | product_dynamic_id  [] | int(11) | YES  | MUL | NULL    |                |
//            | user_id            | int(11) | YES  |     | NULL    |                |
//            | quantity           | int(11) | YES  |     | NULL    |                |
//            +--------------------+---------+------+-----+---------+----------------+

    Long id;

    Long dynamicId;

//    Long staticId;
    Long categoryId;
    String productName;
    String Description;
    String productImage;
    String color;

    Long merchantId;
    Long stock;
    Long price;

    Long userId;
    Long quantity;
}
