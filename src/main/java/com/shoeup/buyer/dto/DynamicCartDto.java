package com.shoeup.buyer.dto;

public interface DynamicCartDto {
//id | product_dynamic_id | user_id | quantity | static_id | merchant_id | stock | price | size
String getid();
    String getproduct_dynamic_id();
    String getuser_id();
    String getquantity();
    String getstatic_id();
    String getmerchant_id();
    Long getstock();
    Long getprice();
    Long getmsize();

}
