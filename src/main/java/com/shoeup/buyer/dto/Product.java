package com.shoeup.buyer.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {
    Long id;
    Long categoryId;
    String productName;
    String productImage;
    String color;
    String Description;

    Long pId;
    Long staticId;
    Long merchantId;
    Long stock;
    Long price;
    Long size;
}

