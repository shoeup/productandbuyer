package com.shoeup.buyer.Service;

import com.shoeup.buyer.dto.Buy;
import com.shoeup.buyer.entity.OrdersEntity;
import com.shoeup.buyer.entity.ProductDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductDetailsService {
    List<Optional<ProductDetails>> view(Long id);

//    List<OrdersEntity> orderHist(Long userId, Long orderId);

    List<ProductDetails> allProducts();

    ProductDetails getById(Long res);

    List<ProductDetails> showall();

    String buying(Buy buy);
    List<ProductDetails> findByCategory(Long categoryId);
}
