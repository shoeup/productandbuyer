package com.shoeup.buyer.Service;

import com.shoeup.buyer.entity.Email;

public interface EmailService {

    String sendMail(Email email);
}
