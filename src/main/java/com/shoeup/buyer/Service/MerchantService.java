package com.shoeup.buyer.Service;

import com.shoeup.buyer.dto.MerchantEntry;
import com.shoeup.buyer.entity.Merchant;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public interface MerchantService {
    Optional<Merchant> getDetails(Long id);
    Boolean addDetails(MerchantEntry merchantEntry);
}
