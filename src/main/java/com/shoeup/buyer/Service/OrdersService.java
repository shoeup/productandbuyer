package com.shoeup.buyer.Service;

import com.shoeup.buyer.dto.Buy;
//import com.shoeup.buyer.dto.CheckOut;
import com.shoeup.buyer.entity.OrdersEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrdersService {
    Boolean add(OrdersEntity ordersEntity);

//    String push(Long userId);

//    List<OrdersEntity> ordersPage(Long userId);

    List<OrdersEntity> find(Long userId);

    OrdersEntity addToOrders(OrdersEntity ordersEntity);
}
