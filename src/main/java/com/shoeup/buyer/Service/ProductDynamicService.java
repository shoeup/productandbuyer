package com.shoeup.buyer.Service;

import com.shoeup.buyer.dto.*;
import com.shoeup.buyer.entity.ProductDetails;
import com.shoeup.buyer.entity.ProductDynamic;
import com.shoeup.buyer.entity.ProductStatic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
@Service
public interface ProductDynamicService {
    List<ProductDynamic> showall();

    List<ProductDynamic> getVariants(Long staticId);

    List<HomePage> findSkuLandingPage();

    List<DynamicCartDto> cartView();

    ProductDynamic addDynamicContent(ProductDynamic productDynamic);

    Boolean deleteProduct(Long pId);

    Boolean addExisting(DynamicDto dynamicDto);

    Boolean updateProduct(DynamicDto dynamicDto);

    List<UpdateDto> findByMerchantId(Integer id);

    List<MerchantCardDto> findbyProductId(Integer pId);

    List<ProductDetails> findCategory(Long categoryId);

    Boolean updateStatus(Long quantity, Long id);


// create view rank_dto as (select d.* from product_dynamic d inner join rank r on
// (r.static_id = d.static_id AND r.merchant_id = d.merchant_id) order by static_id);

}
