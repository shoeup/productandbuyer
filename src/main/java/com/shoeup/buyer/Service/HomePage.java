package com.shoeup.buyer.Service;

public interface HomePage {
    Long getstatic_Id();
    String getCategory_Id();
    String getProduct_Name();
    String getdescription();
    String getproduct_Image();
    String getcolor();
    Long getstock();
    Long getprice();
}
