package com.shoeup.buyer.Service.impl;

import com.shoeup.buyer.Service.HomePage;
import com.shoeup.buyer.Service.ProductDetailsService;
import com.shoeup.buyer.Service.ProductDynamicService;
import com.shoeup.buyer.dto.*;
import com.shoeup.buyer.entity.ProductDetails;
import com.shoeup.buyer.entity.ProductDynamic;
import com.shoeup.buyer.entity.ProductStatic;
import com.shoeup.buyer.feign.FeignService;
import com.shoeup.buyer.repository.ProductDynamicRepo;
import com.shoeup.buyer.repository.ProductStaticRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Service

public class ProductDynamicServiceImpl implements ProductDynamicService {
    @Autowired
    ProductDynamicRepo productDynamicRepo;
    @Autowired
    ProductStaticRepo productStaticRepo;
    @Autowired
    FeignService feignService;
    @Autowired
    ProductDetailsService productDetailsService;

    @Override
    public List<ProductDynamic> showall() {
        return productDynamicRepo.findAll();
    }

    public Collection<ProductDynamic> getSku(){
        return productDynamicRepo.findSkuPrice();
    }

    @Override
    public List<ProductDynamic> getVariants(Long staticId) {
        return productDynamicRepo.findByStaticId(staticId);
    }

    public List<HomePage> findSkuLandingPage(){
        return productDynamicRepo.getSku();
    }
    @Override
    public List<ProductDetails> findCategory(Long categoryId) {
//        return productDynamicRepo.getCategoryList(categoryId);
        return productDetailsService.findByCategory(categoryId);

    }

    @Override
    public Boolean updateStatus(Long quantity, Long id) {
        Long stock=productDynamicRepo.findById(id).get().getStock();
        if(stock>=quantity){
            productDynamicRepo.updateStatus(quantity, id);
            return true;
        }
        return false;
    }



    @Override
    public List<DynamicCartDto> cartView() {
        return productDynamicRepo.getCartView();
    }

    @Override
    public ProductDynamic addDynamicContent(ProductDynamic productDynamic) {
        return productDynamicRepo.save(productDynamic);
    }

    @Override
    public Boolean deleteProduct(Long pId) {
        if(productDynamicRepo.findById(pId)!=null) {
            productDynamicRepo.deleteById(pId);
            return true;
        }
        return false;
    }

    @Override
    public Boolean addExisting(DynamicDto dynamicDto) {
        ProductDynamic productDynamic = new ProductDynamic();
        BeanUtils.copyProperties(dynamicDto,productDynamic);
        productDynamicRepo.save(productDynamic);

        ProductStatic productStatic = productStaticRepo.findByid(productDynamic.getStaticId());
        Product productDto=new Product();
        BeanUtils.copyProperties(productStatic,productDto);
        BeanUtils.copyProperties(productDynamic,productDto);

        feignService.addProduct(productDto);
        return true;
//        return productDynamic.getId();
    }

    @Override
    public Boolean updateProduct(DynamicDto dynamicDto) {
        ProductDynamic productDynamic = new ProductDynamic();
        BeanUtils.copyProperties(dynamicDto, productDynamic);
        productDynamicRepo.save(productDynamic);
        return true;

    }

    @Override
    public List<UpdateDto> findByMerchantId(Integer id) {
        List<String> sd = productDynamicRepo.getProducts(id);
        List<UpdateDto> role = new ArrayList<>();
        for (String s : sd) {
            String[] arrOfStr = s.split(",");
            role.add(new UpdateDto(Long.parseLong(arrOfStr[0]), arrOfStr[1], arrOfStr[2], Long.parseLong(arrOfStr[3]), Long.parseLong(arrOfStr[4]), arrOfStr[5], Long.parseLong(arrOfStr[6]), arrOfStr[7], Long.parseLong(arrOfStr[8])));
        }
        return role;
    }

    @Override
    public List<MerchantCardDto> findbyProductId(Integer pId) {
        List<String> val=  productDynamicRepo.findByProductId(pId);
        List<MerchantCardDto> product = new ArrayList<>();
        for (String s:val){
            String[] arrOfStr = s.split(",");
            product.add(new MerchantCardDto(Long.parseLong(arrOfStr[0]), arrOfStr[1], arrOfStr[2], Long.parseLong(arrOfStr[3]), Long.parseLong(arrOfStr[4]), arrOfStr[5], Long.parseLong(arrOfStr[6]), arrOfStr[7]));
        }
        return product;
    }



//    @Override
//    public TemplateRateDto join1() {
//        return productDynamicRepo.PriceToTemplate();
//    }
    }
