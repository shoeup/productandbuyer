package com.shoeup.buyer.Service.impl;

import com.shoeup.buyer.Service.CartService;
import com.shoeup.buyer.Service.ProductDynamicService;
import com.shoeup.buyer.Service.ProductStaticService;
import com.shoeup.buyer.entity.Cart;
import com.shoeup.buyer.repository.CartRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    CartRepo cartRepo;
    @Autowired
    ProductStaticService productStaticService;
    @Autowired
    ProductDynamicService productDynamicService;

    @Override
    @Transactional
    public Boolean remove(Long userId, Long dynamicId) {
        Integer integer = cartRepo.deleteByUserIdAndProductDynamicId(userId,dynamicId);
        return integer>0?true:false;
    }

    @Override
    public Boolean addToCart(Cart cart) {
        cartRepo.save(cart);
        return true;
    }

    @Override
    public List<Cart> getByUserId(Long userId) {
        return cartRepo.findByUserId(userId);
    }

    public List<Cart> seeId(Long id) {
        return cartRepo.findByUserId(id);
    }


//    public CartDto send(){
//
//    }

}
