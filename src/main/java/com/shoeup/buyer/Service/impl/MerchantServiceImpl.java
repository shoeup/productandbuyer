package com.shoeup.buyer.Service.impl;

import com.shoeup.buyer.Service.MerchantService;
import com.shoeup.buyer.dto.MerchantEntry;
import com.shoeup.buyer.entity.Merchant;
import com.shoeup.buyer.repository.MerchantRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    MerchantRepo merchantRepo;
    @Override
    public Optional<Merchant> getDetails(Long id) {
        return merchantRepo.findById(id);
    }

    @Override
    public Boolean addDetails(MerchantEntry merchantEntry) {
        System.out.println(merchantEntry);
        Merchant merchant = new Merchant();
        merchant.setUserId(merchantEntry.getId());
        merchant.setName(merchantEntry.getName());
        System.out.println(merchant);
        merchantRepo.save(merchant);
        return true;
    }
}
