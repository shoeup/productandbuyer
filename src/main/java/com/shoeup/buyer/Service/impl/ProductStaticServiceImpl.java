package com.shoeup.buyer.Service.impl;

import com.shoeup.buyer.Service.HomePage;
import com.shoeup.buyer.Service.ProductStaticService;
import com.shoeup.buyer.dto.LandingPageDto;
import com.shoeup.buyer.dto.StaticDto;
import com.shoeup.buyer.entity.ProductStatic;
import com.shoeup.buyer.repository.ProductStaticRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductStaticServiceImpl implements ProductStaticService {
    @Autowired
    ProductStaticRepo productStaticRepo;

    @Override
    public ProductStatic save(ProductStatic product) {
        productStaticRepo.save(product);
        return product;
    }

    @Override
    public List<ProductStatic> showall() {

        ProductStatic productStatic = new ProductStatic();
        return productStaticRepo.findAll();
    }

    @Override
    public List<ProductStatic> find() {
//        Long staticId;
//        Long categoryId;
//        String productName;
//        String description;
//        String producImage;
//        String color;
//        Long merchantId;
//        Long stock;
//        Long price;
//        List<ProductStatic> list = productStaticRepo.findAll();
//        LandingPageDto landingPageDto = new
        return productStaticRepo.findAll();
    }

    @Override
    public Optional<ProductStatic> findProduct(Long id) {
        StaticDto staticDto = new StaticDto();


//        BeanUtils.copyProperties(productStaticRepo.findById(id),staticDto);

         return productStaticRepo.findById(id);

    }

    @Override
    public ProductStatic addStaticContent(ProductStatic productStatic) {
        productStaticRepo.save(productStatic);
        return productStatic;
    }

    @Override
    public List<HomePage> findCategory(Long categoryId) {
//        List<ProductStatic> list = new ArrayList<>();
//        list = productStaticRepo.findByCategoryId(categoryId);
//        List<HomePage> list1 = new ArrayList<>();
//        BeanUtils.copyProperties(list, list1);
//        List<HomePage>  productStaticRepo.();

//        List<HomePage> list = productStaticRepo.findByCategoryId(categoryId);
//        return list1;
    return null;
    }

    @Override
    public List<StaticDto> getStaticCon() {
        List<ProductStatic> staticList= productStaticRepo.findAll();
        List<StaticDto> staticDtos=new ArrayList<>();
        for(ProductStatic staticDto:staticList) {
            StaticDto staticDto1=new StaticDto();
            BeanUtils.copyProperties(staticDto,staticDto1);
            staticDtos.add(staticDto1);
        }
        return staticDtos;
    }


//    @Override
//    public List<StaticDto> findCategory(Long categoryId) {
//        List<StaticDto> staticDto = new ArrayList<>();
//        BeanUtils.copyProperties(productStaticRepo.findById(categoryId),staticDto);
//        return staticDto;
//        return productStaticRepo.getByCategoryId(categoryId);
    }

