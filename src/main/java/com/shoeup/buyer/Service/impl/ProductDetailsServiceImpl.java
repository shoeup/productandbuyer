package com.shoeup.buyer.Service.impl;

import com.shoeup.buyer.Service.*;
import com.shoeup.buyer.dto.Buy;
import com.shoeup.buyer.entity.Cart;
import com.shoeup.buyer.entity.Email;
import com.shoeup.buyer.entity.OrdersEntity;
import com.shoeup.buyer.entity.ProductDetails;
import com.shoeup.buyer.repository.ProductDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductDetailsServiceImpl implements ProductDetailsService {

    @Autowired
    ProductDetailsRepo productDetailsRepo;
    @Autowired
    CartService cartService;
    @Autowired
    OrdersService ordersService;
    @Autowired
    EmailService emailService;
    @Autowired
    ProductDynamicService productDynamicService;

    @Override
    public List<Optional<ProductDetails>> view(Long id) {

//        List<ProductDetails> list = new List<ProductDetails>();
        List<Optional<ProductDetails>> res = new ArrayList<>();

        List<Cart> cart = cartService.seeId(id);
        for (int i=0;i<cart.size();i++){
            Cart cart1 = cart.get(i);
            Long dynamicId = cart1.getProductDynamicId();
            res.add(productDetailsRepo.findById(dynamicId));
        }

        return res;
    }

//    @Override
//    public List<OrdersEntity> orderHist(Long userId, Long orderId) {
//        List<Optional<ProductDetails>> res = new ArrayList<>();
//
//        List<OrdersEntity> cart = ordersService.seeId(userId, orderId);
//
//        return cart;
//    }

    @Override
    public List<ProductDetails> allProducts() {
        return productDetailsRepo.findAll();
    }

    @Override
    public ProductDetails getById(Long res) {

        return productDetailsRepo.getById(res);
    }

    @Override
    public List<ProductDetails> showall() {
        return productDetailsRepo.findAll();
    }

    @Override
    public String buying(Buy buy) {
        Long id = buy.getDynamicId();
        ProductDetails productDetails = productDetailsRepo.getById(id);
//        ProductDto productDto =new ProductDto();
//        BeanUtils.copyProperties(productDetails, productDto);
//        List<ProductDto> list= new ArrayList<>();
//        list.add(productDto);
//        OrdersMongoEntityDto ordersMongoEntityDto = new OrdersMongoEntityDto();
//        ordersMongoEntityDto.setCustId(buy.getUserId());
//        ordersMongoEntityDto.setProductDetails(list);
//        ordersMongoEntityDto.setTotalPrice(buy.getPrice());
//        UserOrders ordersMongo = new UserOrders();
//        BeanUtils.copyProperties(ordersMongoEntityDto, ordersMongo);
//        userOrdersRepo.save(ordersMongo);
//        Long quantity= buy.getQuantity();


        Boolean check = productDynamicService.updateStatus(buy.getQuantity(), buy.getDynamicId());
        if (check==false){
            return "Out of Stock";
        }

        Email email = new Email();
        email.setCategoryId(productDetails.getCategoryId());
        email.setDescription(productDetails.getDescription());
        email.setProductName(productDetails.getProductName());
        email.setPrice(productDetails.getPrice());
        email.setRecipient(buy.getEmail());
        email.setMsgBody("Woo hoo! Your order is on its way. Your product details are: \n\n"
                + "Product name: " + email.getProductName()+
                "\nPrice: "+ buy.getPrice() + "\n\n"
        + "Get ready to shoeUp!!!!!!!!!!!!!!");
        email.setSubject("Order placed - shoeUp");
//        OrdersEntity ordersEntity = new OrdersEntity();
//        ordersEntity.setUserId(buy.getUserId());
//        ordersEntity.setProductStaticId(productDetails.getStaticId());
//        ordersEntity.setQuantity(buy.getQuantity());
//        ordersEntity.setProductDynamicId(productDetails.getDynamicId());
//        ordersEntity.setTotalPrice(buy.getPrice());
//        ordersService.ordersRepo.save(ordersEntity);
        String status =  emailService.sendMail(email);
        OrdersEntity ordersEntity = new OrdersEntity();
        ordersEntity.setUserId(buy.getUserId());
        ordersEntity.setProductName(productDetails.getProductName());
        ordersEntity.setProductImage(productDetails.getProductImage());
        ordersEntity.setDescription(productDetails.getDescription());
        ordersEntity.setMerchantId(productDetails.getMerchantId());
        ordersEntity.setQuantity(buy.getQuantity());
        ordersEntity.setTotalPrice(buy.getPrice());
        OrdersEntity res = ordersService.addToOrders(ordersEntity);
//        return integer>0?true:false;
        cartService.remove(buy.getUserId(), buy.getDynamicId());
        return "You order is successfully placed, check your order history for more details";
    }

    public List<ProductDetails> findByCategory(Long categoryId) {
        return productDetailsRepo.findByCategoryId(categoryId);
    }
}
