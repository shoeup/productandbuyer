package com.shoeup.buyer.Service.impl;

import com.shoeup.buyer.Service.CartService;
import com.shoeup.buyer.Service.EmailService;
import com.shoeup.buyer.Service.OrdersService;
import com.shoeup.buyer.dto.Buy;
//import com.shoeup.buyer.dto.CheckOut;
import com.shoeup.buyer.entity.Cart;
import com.shoeup.buyer.entity.OrdersEntity;
import com.shoeup.buyer.repository.OrdersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.shoeup.buyer.BuyerApplication.rand;

@Service
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    OrdersRepo ordersRepo;
    @Autowired
    CartService cartService;
    @Autowired
    EmailService emailService;

    @Override
    public Boolean add(OrdersEntity ordersEntity) {
        ordersRepo.save(ordersEntity);
        Long id = ordersEntity.getId();
//        emailService.sendMail()
        return true;
    }

//    @Override
//    public String push(Long userId) {
//        List<Cart> list = cartService.getByUserId(userId);
//        for (int i=0;i<list.size();i++){
//            Cart cart = list.get(i);
////            product_dynamic_id | user_id | quantity | product_static_id
//            OrdersEntity ordersEntity = new OrdersEntity();
//            ordersEntity.setProductDynamicId(cart.getProductDynamicId());
//            System.out.println(cart.getUserId());
//            ordersEntity.setUserId(cart.getUserId());
//            System.out.println(ordersEntity.getUserId());
//            ordersEntity.setQuantity(cart.getQuantity());
//            ordersEntity.setProductStaticId(cart.getProductStaticId());
//            ordersRepo.save(ordersEntity);
////            add function to delete from cart table
//            //add function to remove
//        }
//        rand++;
//        return "change this string later";
//    }

//    @Override
//    public List<OrdersEntity> ordersPage(Long userId) {
//        return ordersRepo.ordersLanding(userId);
//    }

    @Override
    public List<OrdersEntity> find(Long userId) {
        return ordersRepo.findByUserId(userId);
    }

    @Override
    public OrdersEntity addToOrders(OrdersEntity ordersEntity) {
        return ordersRepo.save(ordersEntity);
    }

//    public List<OrdersEntity> seeId(Long userId, Long orderId) {
//        return ordersRepo.findByUserIdAndOrderId(userId, orderId);
//    }
}
