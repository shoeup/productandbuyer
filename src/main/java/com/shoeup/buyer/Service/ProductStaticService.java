package com.shoeup.buyer.Service;

import com.shoeup.buyer.dto.StaticDto;
import com.shoeup.buyer.entity.ProductStatic;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductStaticService {
    ProductStatic save(ProductStatic product);

    List<ProductStatic> showall();

    List<ProductStatic> find();

    Optional<ProductStatic> findProduct(Long id);

    ProductStatic addStaticContent(ProductStatic productStatic);

    List<HomePage> findCategory(Long categoryId);

    List<StaticDto> getStaticCon();

//    List<StaticDto> findCategory(Long categoryId);
}
