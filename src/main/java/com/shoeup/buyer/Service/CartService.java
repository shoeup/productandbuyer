package com.shoeup.buyer.Service;

import com.shoeup.buyer.entity.Cart;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CartService {

    Boolean remove(Long userId, Long dynamicId);

    Boolean addToCart(Cart cart);

    List<Cart> getByUserId(Long userId);

    List<Cart> seeId(Long id);
}
