package com.shoeup.buyer.feign;

import com.shoeup.buyer.dto.Product;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@org.springframework.cloud.openfeign.FeignClient(value = "feignDemo", url="http://172.16.29.152:8080")
public interface FeignService {
    @PostMapping("/getvalue")
    ResponseEntity<String> addProduct(Product productDto);

    @PostMapping ("/delete")
    ResponseEntity<String> del(@RequestParam Long pId);

}
//public interface FeignService {
//}
