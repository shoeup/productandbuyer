package com.shoeup.buyer.controller;

import com.shoeup.buyer.Service.*;
//import com.shoeup.buyer.dto.CheckOut;
import com.shoeup.buyer.dto.*;
import com.shoeup.buyer.entity.*;
import com.shoeup.buyer.feign.FeignService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {

    @Autowired
    ProductDynamicService productDynamicService;
    @Autowired
    ProductStaticService productStaticService;
    @Autowired
    CartService cartService;
    @Autowired
    EmailService emailService;
    @Autowired
    MerchantService merchantService;
    @Autowired
    OrdersService ordersService;
    @Autowired
    ProductDetailsService productDetailsService;
    @Autowired
    FeignService feignService;

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @PostMapping("/add")
    public Boolean add(@RequestBody Product product) {
        ProductStatic productStatic = new ProductStatic();
        productStatic.setId(product.getId());
        productStatic.setCategoryId(product.getCategoryId());
        productStatic.setProductImage(product.getProductImage());
        productStatic.setProductName(product.getProductName());
        productStatic.setColor(product.getColor());
        productStatic.setDescription(product.getDescription());
        ProductStatic productStatic1 = productStaticService.addStaticContent(productStatic);

        ProductDynamic productDynamic = new ProductDynamic();
        productDynamic.setStaticId(productStatic1.getId());
        productDynamic.setMerchantId(product.getMerchantId());
        productDynamic.setStock(product.getStock());
        productDynamic.setPrice(product.getPrice());
        productDynamic.setSize(product.getSize());

        ProductDynamic res = productDynamicService.addDynamicContent(productDynamic);
        Product productDto=new Product();
        BeanUtils.copyProperties(productStatic1,productDto);
        BeanUtils.copyProperties(res,productDto);


        feignService.addProduct(productDto);
        return true;

    }
    @PostMapping("/addExisting")
    public Boolean addExisting(@RequestBody DynamicDto dynamicDto){
        Boolean res = productDynamicService.addExisting(dynamicDto);
//        productDetailsService.getById(res);
        return res;
    }

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @DeleteMapping("/delete")
    public Boolean deleteProduct(@RequestParam Long pId) {
        Boolean res = productDynamicService.deleteProduct(pId);
        feignService.del(pId);
        return res;
    }

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @PutMapping("/update")
    public Boolean updateProduct(@RequestBody DynamicDto dynamicDto ){
        Boolean res = productDynamicService.updateProduct(dynamicDto);
        return res;

    }

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @GetMapping("/findbyId") //findByMerchantId
    public List<UpdateDto> findByMerchantId(@RequestParam Integer id){
        return productDynamicService.findByMerchantId(id);
    }

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @GetMapping("/getProductById")
    public List<MerchantCardDto> findbyProductId(@RequestParam Integer pId){
        return productDynamicService.findbyProductId(pId);
    }

//    @GetMapping("/test")
//    @CrossOrigin(origins = "http://172.16.29.129:3000")
//    ResponseEntity <ProductStatic> saveall(@RequestBody ProductDto template)(
//    ProductDto dto){
//        ResponseEntity<ProductStatic> res = new ResponseEntity(productStaticService.save(dto.getProduct()), HttpStatus.OK);
//        return res;
//    }

//    @GetMapping("/home")
//    @CrossOrigin(origins = "http://172.16.29.129:3000")
//    ResponseEntity<List<ProductStatic>> showall(){
//        ResponseEntity<List<ProductStatic>> showAll = new ResponseEntity<>(productStaticService.showall(), HttpStatus.OK);
//        return  showAll;
//    }
//    @GetMapping("/test")
//    ResponseEntity <TemplateRateDto> addStatic(){
//        ResponseEntity<TemplateRateDto> templateRateDto = new ResponseEntity(productDynamicService.join1(), HttpStatus.OK);
//        return  templateRateDto;
//    }

//    @GetMapping("/product")
//    @CrossOrigin(origins = "http://172.16.29.129:3000")
//    ResponseEntity<List<ProductDynamic>> productClick(@RequestParam Long staticId){
//        ResponseEntity<List<ProductDynamic>> showAllSellers = new ResponseEntity(productDynamicService.getVariants(staticId), HttpStatus.OK);
//        return  showAllSellers;
//    }
//    @GetMapping("/find")
//    public List<ProductStatic> findAll(){
//        return productStaticService.find();
//    }
    @GetMapping("/check")
    public JsonEntity check(@RequestBody JsonEntity jsonEntity){
        return jsonEntity;
    }

    @GetMapping("/find")
    @CrossOrigin(origins = "http://172.16.29.149:3000")
    public List<ProductStatic> findAll(){
        return productStaticService.find();
    }

    @GetMapping("/product")
    @CrossOrigin(origins = "http://172.16.29.149:3000")
    public Optional<ProductStatic> findProduct(@RequestParam Long id){

        return productStaticService.findProduct(id);
    }

//    @GetMapping("/productclick")
//    @CrossOrigin(origins = "http://172.16.29.149:3000")
//    public Optional<ProductStatic> clickProduct(@RequestParam Long dynamicId,Long staticId){
//
//        return productStaticService.findProduct(id);
//    }

    @PostMapping("/addtocart")
    @CrossOrigin(origins = "http://172.16.29.149:3000")
    public Boolean addToCart(@RequestBody Cart cart){
        return cartService.addToCart(cart);
    }

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @GetMapping("/getStatic")
    public List<StaticDto> getStaticCon(){
        return productStaticService.getStaticCon();
    }

//    @GetMapping("/home")
//    @CrossOrigin(origins = "http://172.16.29.149:3000")
//    public List<HomePage> landingPage(){
//        return productDynamicService.findSkuLandingPage();
//    }

    @GetMapping("/home")
    @CrossOrigin(origins = "http://172.16.29.149:3000")
    public List<ProductDetails> landingPage(){
        return productDetailsService.showall();
    }
    @GetMapping("/category")
    @CrossOrigin(origins = "http://172.16.29.149:3000")
    public List<ProductDetails> findCategory(@RequestParam Long categoryId){

        return productDynamicService.findCategory(categoryId);
    }

    @GetMapping("/merchant")
    @CrossOrigin(origins = "http://172.16.29.149:3000")
    public Optional<Merchant> details(@RequestParam Long id){
        return merchantService.getDetails(id);
    }

    @PostMapping("/addmerchantfromdb")
    public Boolean entry(@RequestBody MerchantEntry merchantEntry) {
        return merchantService.addDetails(merchantEntry);
    }

    @CrossOrigin(origins = "http://172.16.29.149:3000")
    @GetMapping("/dy")
    public List<ProductDynamic> showDynamic(){
        return productDynamicService.showall();
    }

    @PutMapping("/updateStock")
    public Boolean updateStatus(@RequestParam Long quantity, Long id){
        return productDynamicService.updateStatus(quantity,id);

    }
//    @CrossOrigin(origins = "http://172.16.29.149:3000")
//    @PostMapping("/buy")
//    public Boolean addToOrders(@RequestBody OrdersEntity ordersEntity){
//        ordersService.add(ordersEntity);
////        String res = emailService.sendMail(Email email);
//
//        return true;
//    }

    @CrossOrigin(origins = "http://172.16.29.149:3000")
    @PostMapping("/buy")
    public String addToOrders(@RequestBody Buy buy){
//        String res = emailService.sendMail(Email email);

        return productDetailsService.buying(buy);

    }

    @CrossOrigin(origins = "http://172.16.29.149:3000")
    @GetMapping("/orders")
    public List<OrdersEntity> seeOrders(@RequestParam Long userId){
//        String res = emailService.sendMail(Email email);

        return ordersService.find(userId);

    }
//    public List<ProductDynamic> algo(){
//
//    }

//    @GetMapping("/test")
//    ResponseEntity <List<ProductDynamic>> showall(){
//        ResponseEntity<List<ProductDynamic>> showAll = new ResponseEntity<>(productDynamicService.showall(), HttpStatus.OK);
//        return showAll;
//    }

@CrossOrigin(origins = "http://172.16.29.149:3000")
    @PostMapping("/sendMail")
    public String sendMail(@RequestBody Email email){

        String res = emailService.sendMail(email);
        return res;
    }

    @CrossOrigin(origins = "http://172.16.29.149:3000")
    @GetMapping("/cartview")
    public List<Optional<ProductDetails>> cartView(@RequestParam Long id){
        return productDetailsService.view(id);
    }

//    @GetMapping("/ordersview")
//    public List<OrdersEntity> ordersView(@RequestParam Long userId, Long orderId){
//        return productDetailsService.orderHist(userId, orderId);
//    }
//@CrossOrigin(origins = "http://172.16.29.149:3000")
//    @GetMapping("/orderlanding")
//    public List<OrdersEntity> ordersLandingPage(@RequestParam Long userId){
//        return ordersService.ordersPage(userId);
//    }

//    @GetMapping("/cart1")
//    public List<DynamicCartDto> cartView(){
//        return productDynamicService.cartView();
//    }

//@CrossOrigin(origins = "http://172.16.29.149:3000")
//@PostMapping("/checkout")
//    public String checkOut(@RequestParam Long id){
//        return ordersService.push(id);
//    }
//
////    @GetMapping("")

    @PutMapping("/updatecart")
    @Transactional
    public Boolean removeFromCart(@RequestParam Long userId, Long dynamicId){
        return cartService.remove(userId, dynamicId);
    }



}
