package com.shoeup.buyer.repository;

import com.shoeup.buyer.entity.ProductDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDetailsRepo extends JpaRepository<ProductDetails, Long> {

    List<ProductDetails> findByCategoryId(Long categoryId);
}
