package com.shoeup.buyer.repository;

import com.shoeup.buyer.Service.HomePage;
import com.shoeup.buyer.dto.StaticDto;
import com.shoeup.buyer.entity.ProductStatic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductStaticRepo extends JpaRepository<ProductStatic, Long> {

//    List<ProductStatic> getByCategoryId(Long categoryId);
    ProductStatic findByid(Long staticId);
    List<ProductStatic> findByCategoryId(Long categoryId);
}
