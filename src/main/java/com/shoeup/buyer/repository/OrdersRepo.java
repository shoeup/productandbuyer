package com.shoeup.buyer.repository;

import com.shoeup.buyer.entity.OrdersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepo extends JpaRepository<OrdersEntity, Long> {


//    List<OrdersEntity> findByUserIdAndOrderId(Long userId, Long orderId) ;

//    @Query("SELECT c.year, COUNT(c.year) FROM Comment AS c GROUP BY c.year ORDER BY c.year DESC")
//    List<Object[]> countTotalCommentsByYear();

//    @Query("SELECT u FROM User u WHERE u.status = :status and u.name = :name")
//    User findUserByStatusAndNameNamedParams(
//            @Param("status") Integer status,
//            @Param("name") String name);

//    @Query("SELECT new com.example.placement.dto.Placed( p.guidedBy, count(*))"
//            + " FROM placement AS p GROUP BY p.guidedBy")
//    List<Placed> groupedData();
//
//    @Query("select new com.shoeup.buyer.entity.OrdersEntity(o.order_id)) " + " from orders o where o.user_id= :user group by o.order_id;")
//    List<OrdersEntity> ordersLanding(@Param("user") Long userId);

//    @Query(value = "select * from orders where user_id= :user group by order_id;", nativeQuery = true)
//    List<OrdersEntity> ordersLanding(@Param("user") Long userId);

    List<OrdersEntity> findByUserId(Long userId);
}
