package com.shoeup.buyer.repository;

import com.shoeup.buyer.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepo extends JpaRepository<Cart, Long> {
    List<Cart> findByUserId(Long id);

    Integer deleteByUserIdAndProductDynamicId(Long userId, Long dynamicId);
}
