package com.shoeup.buyer.repository;

import com.shoeup.buyer.Service.HomePage;
import com.shoeup.buyer.dto.DynamicCartDto;
import com.shoeup.buyer.dto.DynamicDto;
import com.shoeup.buyer.dto.LandingPageDto;
import com.shoeup.buyer.entity.ProductDynamic;
import com.shoeup.buyer.entity.ProductStatic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductDynamicRepo extends JpaRepository<ProductDynamic, Long> {
//    List<Employee> findByFirstName(String firstName);
    List<ProductDynamic> findByStaticId(Long staticId);
//select d.* from product_dynamic d inner join rank r on (r.static_id = d.static_id AND r.merchant_id = d.merchant_id);
//    @Query("select new com.shoeup.buyer.entity.ProductDynamic (d.*)" + " from product_dynamic d inner join rank r on (r.static_id = d.static_id AND r.merchant_id = d.merchant_id);")
//    TemplateRateDto PriceToTemplate();

//    @Query(value = "select d.* from product_dynamic d inner join rank r on (r.static_id = d.static_id AND r.merchant_id = d.merchant_id);",
//            nativeQuery = true)
//    Collection<DynamicDto> findSkuPrice();
//    @Query(value = "select r.static_id, s.category_id, s.product_name, s.description, s.product_image, s.color, r.merchant_id, r.stock, r.price from product_static s inner join rank_dto r on (r.static_id = s.id);",
//    nativeQuery = true)
//    Collection<LandingPageDto> getSku();

    @Query(value = "select d.* from product_dynamic d inner join rank r on (r.static_id = d.static_id AND r.merchant_id = d.merchant_id);",
            nativeQuery = true)
    List<ProductDynamic> findSkuPrice();

    @Query(value = "select r.static_id, s.category_id, s.product_name, s.description, s.product_image, s.color, r.merchant_id, r.stock, r.price from product_static s inner join rank_dto r on (r.static_id = s.id);",
            nativeQuery = true)
    List<HomePage> getSku();

//    @Query(value = "select r.static_id, s.category_id, s.product_name, s.description, s.product_image, s.color, r.merchant_id, r.stock, r.price from product_static s inner join rank_dto r on (r.static_id = s.id) where s.category_id = :categoryId",
//            nativeQuery = true)
//    List<HomePage> getCategoryList(@Param("categoryId") Long categoryId);

//    id | product_dynamic_id | user_id | quantity | static_id | merchant_id | stock | price | size
    @Query(value = "select c.*, d.static_id, d.merchant_id, d.stock, d.price, d.size from cart c inner join product_dynamic d on c.product_dynamic_id = d.id;", nativeQuery = true)
    List<DynamicCartDto> getCartView();

    @Query(value ="select d.id,p.product_name,p.description,d.price,d.stock,p.product_image,d.size,p.color, d.static_id from product_static AS p join product_dynamic AS " +
            "d on p.id=d.static_id where d.merchant_id=?1", nativeQuery = true)
    List<String> getProducts(Integer id);

    @Query(value = "select d.id,p.product_name,p.description,d.price,d.stock,p.product_image,d.size,p.color from product_static AS p " +
            "join product_dynamic d on p.id=d.static_id where d.id=?1 ",nativeQuery = true)
    List<String> findByProductId(Integer pId);

    @Query(value = "update product_dynamic p set p.stock = p.stock-?1 where p.id= ?2", nativeQuery = true)
    Boolean updateStatus(Long quantity, Long id);

//    List<HomePage> findByCategoryId(Long categoryId);

//    List<LandingPageDto> getSku();

//    @Query(value = "SELECT g.*, gm.* FROM group g LEFT JOIN group_members gm ON g.group_id = gm.group_id and gm.user_id = :userId WHERE g.group_id = :groupId", nativeQuery = true
//
//            String[][] getGroupDetails(@Param("userId") Integer , @Param("staticId") Integer static_id);)
}


