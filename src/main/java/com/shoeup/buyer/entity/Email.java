package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Email {
//    String recipient;
//    String msgBody;
//    String subject;
//    String productName;
//    String Description;
//    String productImage;
//    String color;
    String recipient;
    String msgBody;
    String subject;
    Long categoryId;
    String productName;
    String productImage;
    String color;
    String description;
    Long merchantId;
    Long price;
    Long size;
}
