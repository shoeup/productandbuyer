package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity(name = "merchant")
public class Merchant {
//+----+---------+-----------------+-----------------+
//| id | user_id | gst             | merchant_rating
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(name = "user_id")
    Long userId;
    @Column(name = "merchant_rating")
    Long merchantRating;
    @Column(name = "name")
    String name;
}
