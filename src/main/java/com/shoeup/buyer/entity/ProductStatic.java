package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "product_static")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductStatic {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "category_id")
    Long categoryId;

    @Column(name = "product_name")
    String productName;

    @Column(name = "description")
    String Description;

    @Column(name="product_image")
    String productImage;

    @Column(name = "color")
    String color;
    @OneToMany(targetEntity = ProductDynamic.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "static_id",referencedColumnName = "id")
    private List<ProductDynamic> productDynamicList;
}
