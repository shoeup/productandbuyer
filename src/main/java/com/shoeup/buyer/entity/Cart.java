package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "cart")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(name = "product_dynamic_id")
    Long productDynamicId;
    @Column(name = "user_id")
    Long userId;
    @Column(name = "quantity")
    Long quantity;
    @Column(name = "product_static_id")
    Long productStaticId;
    @Column(name = "price")
    Long price;
}
