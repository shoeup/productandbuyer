package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "product_dynamic")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDynamic {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "static_id")
    Long staticId;

    @Column(name="merchant_id")
    Long merchantId;

    @Column(name = "stock")
    Long stock;

    @Column(name = "price")
    Long price;

    @Column(name = "size")
    Long size;
}
