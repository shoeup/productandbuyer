package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "product_details")
public class ProductDetails {

    @Column(name = "static_id")
    Long staticId;
    @Id
    @Column(name = "dynamic_id")
    Long dynamicId;
    @Column(name = "category_id")
    Long categoryId;
    @Column(name = "product_name")
    String productName;
    @Column(name = "product_image")
    String productImage;
    @Column(name = "color")
    String color;
    @Column(name = "description")
    String description;
    @Column(name = "merchant_id")
    Long merchantId;
    @Column(name = "stock")
    Long stock;
    @Column(name = "price")
    Long price;
    @Column(name = "size")
    Long size;

}
