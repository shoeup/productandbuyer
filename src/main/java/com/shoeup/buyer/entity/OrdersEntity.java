package com.shoeup.buyer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name = "hist_orders")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class OrdersEntity {
        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        Long id;

        @Column(name = "user_id")
        Long userId;

        @Column(name = "timestamp")
        @CreationTimestamp
        Timestamp timestamp;

        @Column(name = "product_name")
        String productName;

        @Column(name = "description")
        String description;

        @Column(name = "product_image")
        String productImage;

        @Column(name = "merchant_id")
        Long merchantId;

        @Column(name = "quantity")
        Long quantity;

        @Column(name = "total_price")
        Long totalPrice;
}
